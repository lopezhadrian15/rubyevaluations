class UsersController < ApplicationController
   
    def index
   @users = User.all
  end
  
  def new
     @user = User.new
  end
 
   def edit
    @user = User.find(params[:id])
   end
  
  def create
   @user = User.new(user_params)
   if @user.save
    flash[:notice] = "User was succesfully created"
    redirect_to user_path(@user)
    else
     render 'new'
   end
  end
   
   
  def update
   @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:notice] = "User was succesfully updated"
      redirect_to user_path(@user)
       else
        render 'edit'
     
    end
  end
  
  def destroy
   @user = User.find(params[:id])
   @user.destroy
   flash[:notice] = "User was succesfully deleted"
   redirect_to users_path
   
  end
  
  def show
   @user = User.find(params[:id])
   
  end 
    
end


 private 
  def user_params
    params.require(:user).permit(:firstname, :lastname,:email) 
 end

