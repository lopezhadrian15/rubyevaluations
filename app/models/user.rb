class User < ActiveRecord::Base
    validates :firstname, presence:true, length:{minimum:3,maximum:12}
    validates :lastname, presence:true, length:{minimun:3,maximum:20}
    validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
end